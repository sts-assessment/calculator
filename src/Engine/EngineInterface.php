<?php


namespace STS\CalculatorBundle\Engine;


interface EngineInterface
{
    public function calculate(string $expr): float;
}
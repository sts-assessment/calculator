<?php
namespace STS\CalculatorBundle\Engine;


use STS\CalculatorBundle\Exceptions\CalculationException;
use STS\CalculatorBundle\Exceptions\ValidationException;

class EvalEngine implements EngineInterface
{
    public function calculate(string $expr): float
    {
        $normalized = $this->normalize($expr);
        $this->validate($normalized);
        try {
            $result = eval("return $normalized;");
        } catch (\Throwable $e) {
            throw new CalculationException($e->getMessage());
        }

        return $result;
    }

    /**
     * Normalize expression string not removing meaningful data
     *
     * @param string $expr
     * @return string
     */
    public function normalize(string $expr): string
    {
        // Remove whitespaces
        $result = preg_replace('/\s/', '', $expr);
        // Remove leading zeros to prevent conversion from octal to decimal
        $result = preg_replace('/(?<![\d\.])([0]+)(\d+)/', '${2}', $result);

        return $result;
    }

    /**
     * Validate expression and throw exception in case validation failed
     *
     * @param $expr
     * @throws ValidationException
     */
    public function validate($expr)
    {
        if (!preg_match('/^[\d\.\+\-\*\/]*$/', $expr)) {
            throw new ValidationException('Expression contains unrecognized symbols');
        }
    }
}
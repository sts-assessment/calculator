<?php
namespace STS\CalculatorBundle;


use STS\CalculatorBundle\Engine\EvalEngine;

class Calculator
{
    public $engine;

    public function __construct()
    {
        $this->engine = new EvalEngine();
    }

    public function calculate(string $expr): float {
        return $this->engine->calculate($expr);
    }
}
<?php
namespace STS\CalculatorBundle\Tests;


use STS\CalculatorBundle\Engine\EvalEngine;
use STS\CalculatorBundle\Exceptions\CalculationException;
use STS\CalculatorBundle\Exceptions\ValidationException;

class EvalEngineTest extends \PHPUnit\Framework\TestCase
{
    public function allowedSymbolsProvider()
    {
        return [
            [''],
            ['1'],
            ['-11'],
            ['-010'],
            ['11'],
            ['11+1'],
            ['11*-2.14'],
            ["12/-10.0"],
        ];
    }

    public function unallowedSymbolsProvider()
    {
        return [
            ['0b'],
            ['(1)'],
            ['0b10-14']
        ];
    }

    /**
     * @dataProvider allowedSymbolsProvider
     */
    public function testValidateAllowedSymbols($expression)
    {
        $this->assertNull((new EvalEngine)->validate($expression));
    }

    /**
     * @dataProvider unallowedSymbolsProvider
     */
    public function testValidateUnallowedSymbols($expression)
    {
        $this->expectException(ValidationException::class);
        (new EvalEngine)->validate($expression);
    }

    public function normalizeProvider()
    {
        return [
            ["12003-0+10-10.014-932", "12003 -   0 + 0000010 - 10.014-\t\t0932"],
            ["2*-10.0", "2*-010.0"],
            ["-2+4", "-2+4"],
        ];
    }

    /**
     * @dataProvider normalizeProvider
     */
    public function testNormalizeExpression($expected, $expression)
    {
        $this->assertEquals($expected, (new EvalEngine)->normalize($expression));
    }

    public function calculateValidProvider()
    {
        return [
            [-6, "2*-3"]
        ];
    }

    /**
     * @dataProvider calculateValidProvider
     */
    public function testCalculateValid($expected, $expression)
    {
        $this->assertEquals($expected, (new EvalEngine)->calculate($expression));
    }

    public function calculateInvalidProvider()
    {
        return [
            ['10+/1'],
            ['2/0'],
        ];
    }

    /**
     * @dataProvider calculateInvalidProvider
     */
    public function testCalculateInvalid($expression)
    {
        $this->expectException(CalculationException::class);
        (new EvalEngine)->calculate($expression);
    }
}